# Docker node Vagrantfile #

This repository contains a Vagrantfile that will setup an ubuntu xenial box and install docker in it.


### How do I get set up? ###

* Install VirtualBox
* Install Vagrant
* Clone this repository and cd into it

```bash
vagrant up
vagrant ssh
docker run --rm hello-world
```
